const onoff = require('onoff')
const Gpio = onoff.Gpio

const pin1 = new Gpio(1, 'out')
const pin2 = new Gpio(2, 'out')

const button = new Gpio(3, 'both') // has a watch method

console.log(onoff)
console.log(Gpio)
